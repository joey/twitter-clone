'use strict'
import File from 'node:fs/promises'
import fs from 'node:fs'
import readline from 'node:readline'
import Stream from 'node:stream'
import cline from 'cline'
import {Adapter, TextMessage} from 'hubot'
import EnvParser from '../../lib/EnvParser.mjs'
import { randomUUID } from 'node:crypto'
import WebSetup from '../../lib/WebSetup.mjs'


const historySize = process.env.HUBOT_SHELL_HISTSIZE != null ? parseInt(process.env.HUBOT_SHELL_HISTSIZE) : 1024
const historyPath = '.hubot_history'
const BODY_LIMIT = 2000
class Shell extends Adapter {
    #hasConnected = false
    constructor(robot, config){
        super(robot)
        this.config = config
    }
    async send (envelope, ...strings) {
        let responses = []
        strings.forEach(s => this.robot.logger.debug(s))
        if(this.cli) this.cli.prompt(`${this.robot.name}> `)
        return responses
    }

    emote (envelope, ...strings) {
        strings.map(str => this.send(envelope, `* ${str}`))
    }

    async reply (envelope, ...strings) {
        return await this.send(envelope, ...strings)
    }
    createMessage(context){
        const user = Object.assign({
            room: context.channelId,
            name: context.author.username,
            message: context
        }, context.author)
        return new TextMessage(user, context.content, context.id, context)
    }
    run(){
        const execute = async message => {
            if(message.author.bot) return
            if(message.mentions && !message.mentions.users.find(u => u.id == this.client.user.id)) return
            const botName = this.robot.alias || this.robot.name
            const content = message.content.replace(`<@${this.client?.user?.id}> `, `${botName} `)
            const user = Object.assign({
                room: message.channelId,
                name: message.author.username,
                message: message
            }, message.author)
            const hubotMessage = new TextMessage(user, content, message.id, message)
            return await this.receive(hubotMessage)
        }
        if(process.env.NODE_ENV == 'development') {
            this.buildCli()
            loadHistory((error, history) => {
                if (error) {
                    this.robot.logger.debug(error.message)
                }

                this.cli.history(history)
                this.cli.interact(`${this.robot.name}> `)
                if(!this.#hasConnected){
                    this.#hasConnected = true
                    this.emit('connected')
                }
                return
            })
            this.cli.command('*', async input => {
                await execute({
                    content: input,
                    channelId: 'shell',
                    author: {
                        username: 'owner',
                        bot: false
                    },
                    id: randomUUID(),
                    reply: message => {
                        this.send(null, message)
                    }
                })
            })
        } else {
            if(!this.#hasConnected){
                this.#hasConnected = true
                this.emit('connected')
            }
            return
        }
    }

    shutdown () {
        this.robot.shutdown()
        this.robot.logger.info('shutting down')
        return process.exit(0)
    }

    buildCli () {
        this.cli = cline()

        this.cli.command('*', input => {
            let userId = process.env.HUBOT_SHELL_USER_ID || '1'
            if (userId.match(/A\d+z/)) {
                userId = parseInt(userId)
            }

            const userName = process.env.HUBOT_SHELL_USER_NAME || 'Shell'
            const user = this.robot.brain.userForId(userId, { name: userName, room: 'Shell' })
            this.receive(new TextMessage(user, input, 'messageId'))
        })

        this.cli.command('history', () => {
            Array.from(this.cli.history()).map(item => this.robot.logger.debug(item))
        })

        this.cli.on('history', item => {
            if (item.length > 0 && item !== 'exit' && item !== 'history') {
                File.appendFile(historyPath, `${item}\n`).then(error => {
                    if (error) {
                        this.robot.emit('error', error)
                    }
                })
                .catch(e => this.robot.emit('error', e))
            }
        })

        this.cli.on('close', () => {
            let history, i, item, len

            history = this.cli.history()

            if (history.length <= historySize) {
                return this.shutdown()
            }

            const startIndex = history.length - historySize
            history = history.reverse().splice(startIndex, historySize)
            const fileOpts = {
                mode: 0x180
            }

            const outstream = fs.createWriteStream(historyPath, fileOpts)
            outstream.on('finish', this.shutdown.bind(this))

            for (i = 0, len = history.length; i < len; i++) {
                item = history[i]
                outstream.write(item + '\n')
            }

            outstream.end(this.shutdown.bind(this))
        })
    }
}

export default async (robot)=>{
    let data = {}
    let config = {}
    try{
        data = await File.readFile('.env', 'utf8')
        config = EnvParser.parse(data)
    }catch(e){

    }
    config = Object.assign(config, process.env)
    robot.config = {
        TIME_ZONE: 'CST',
        LANG: 'en-US'
    }
    await WebSetup(robot, config)
    return new Shell(robot, config)
}

// load history from .hubot_history.
//
// callback - A Function that is called with the loaded history items (or an empty array if there is no history)
function loadHistory (callback) {
  if (!fs.existsSync(historyPath)) {
    return callback(new Error('No history available'))
  }

  const instream = fs.createReadStream(historyPath)
  const outstream = new Stream()
  outstream.readable = true
  outstream.writable = true

  const items = []

  readline.createInterface({ input: instream, output: outstream, terminal: false })
    .on('line', function (line) {
      line = line.trim()
      if (line.length > 0) {
        items.push(line)
      }
    })
    .on('close', () => callback(null, items))
    .on('error', callback)
}
