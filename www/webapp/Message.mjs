class Message {
    constructor(text, from, id, occurred = Date.now()){
        this.text = text
        this.from = from
        this.occurred = occurred
        this.id = id
    }
}
export default Message