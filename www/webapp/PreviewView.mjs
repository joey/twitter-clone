import NotificationCenter from '/lib/NotificationCenter.mjs'
import {Events} from '/webapp/Events.mjs'
export default class PreviewView {
    constructor(container, model, delegate = {
        window: window ?? {addEventListener: (event, listener, doesBubble) => {}},
        messageWasSubmitted(message){}
    }){
        this.container = container
        this.model = model
        this.delegate = delegate
        this.text = this.container.querySelector('.text')
        this.container.querySelector('.avatar img').src = this.model.from.avatar
        this.model.observe('text', this.textDidChange.bind(this))
        NotificationCenter.subscribe(Events.HAS_STARTED_TYPING, this.show.bind(this), null)
		NotificationCenter.subscribe(Events.HAS_STOPPED_TYPING, this.hide.bind(this), null)
    }
    textDidChange(key, old, value){
        this.text.innerHTML = this.delegate.window.markdownit().render(value)
    }
    show(){
        this.container.style.display = ''
    }
    hide(){
        this.container.style.display = 'none'
    }
}