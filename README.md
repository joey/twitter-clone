# Local-first Twitter Clone

Stores and reads posts to and from `indexdb`.

My current plan is to sync posts to a central server and utilize WebRTC for peer-to-peer communications. But everyone has a plan until they get punched in the mouth. So stay tuned.

Btw, I'm using my version of [Hubot](https://github.com/joeyguerra/hubot) to build on top of. It runs the shell adapter when `NODE_ENV=development` and [WebSetup](/lib/WebSetup.mjs) generates a static site for the webapp.

# Start

## [Corepack](https://github.com/nodejs/corepack#readme)

```sh
corepack enable
corepack prepare pnpm@7.18.2 --activate
```

## Environment

Create a file called `.env` at the root of this repository and add the following:

```sh
COOKIE_SECRET=yoursecrethereshhhh!
```

Replace `yoursecrethereshhhh!` with something else (don't use spaces for now).

Then execute the following commands.

```sh
pnpm i
NODE_ENV=development pnpm run start:local
```

Then go to [localhost](http://localhost:8080)

### Public and Private Keys

Generate a public and private key pair using the following (Linux and Mac):

```sh
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

## Run tests

```sh
pnpm

# External Traffic Forwarding for local developement

Using CloudFlare's `cloudflared` tunnelling.

```sh
cloudflared tunnel login
cloudflared tunnel create <tunnel name>
cloudflared tunnel list
# Create the CNAME
cloudflared tunnel route dns <tunnel name or UUID> <domain name>
cloudflared tunnel run <domain name>
```