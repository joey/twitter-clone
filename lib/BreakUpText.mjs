
export default function* breakUp(text, boundary){
    let counter = 0
    while(counter <= text.length){
        yield text.substring(counter, counter + boundary)
        counter = counter + boundary
    }
}