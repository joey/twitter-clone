
import MethodOverride from 'method-override'
import express from 'express'
import session from 'express-session'
import {readFiles} from '../lib/ReadFiles.mjs'
import File from 'node:fs/promises'
import fs, {createReadStream, createWriteStream} from 'node:fs'
import {ObservableArray} from '../lib/Observable.mjs'
import MemoryStore from '../lib/MemoryStore.mjs'
import {WebSocketServer, WebSocket} from 'ws'
import { randomUUID } from 'node:crypto'

import MarkdownIt from "markdown-it"
import Meta from "markdown-it-meta"
import { fileURLToPath } from 'node:url'

import {load as cheerio} from 'cheerio'
import handlebars from 'handlebars'

import {verify, getFromHeaders} from './HttpSignature.mjs'

const __dirname = fileURLToPath(new URL('.', import.meta.url)).replace('/lib/', '')

const markdown = new MarkdownIt({
	html: true,
	linkify: true,
	typographer: true
}).use(Meta)

const posts = []
const visitStream = new ObservableArray()
class Event {
    constructor(kind, id, occurred, recorded, who, what, originatingSystemId){
        this.envelope = {
            kind, id, occurred, recorded, who, what, originatingSystemId
        }
    }
}
class AddVisitEvent extends Event {
    constructor(sessionId, id, occurred, recorded, who, what, originatingSystemId){
        super('https://example.com/visits/add', id, occurred, recorded, who, what, originatingSystemId)
        this.sessionId = sessionId
    }
}

visitStream.observe('push', (key, old, value)=>{
    //console.log(value)
})

const store = new MemoryStore()
const SESSIONS_DIR = './.data/sessions'
try{
    fs.statSync(SESSIONS_DIR)
}catch(e){
    fs.mkdirSync(SESSIONS_DIR, {recursive: true})
}

let sessionsJob = setInterval(async ()=>{
    let sessions = null
    store.all(async (err, s)=>{
        sessions = s
    })
    for await (let session of sessions){
        try{
            await File.stat(`${SESSIONS_DIR}/${session.sessionID}.ndjson`)
        }catch(e){
            await File.writeFile(`${SESSIONS_DIR}/${session.sessionID}.ndjson`, `${JSON.stringify(session)}\n`, 'utf-8')
        }
    }
    const toExpire = new Set()
    for await (let file of await File.readdir(SESSIONS_DIR)){
        const data = await File.readFile(`${SESSIONS_DIR}/${file}`, 'utf-8')
        const lastLine = data.split('\n').filter(t => t.trim().length > 0).pop()
        const lastVersion = JSON.parse(lastLine)
        const expires = new Date(lastVersion.cookie.expires)
        if(expires && expires < Date.now()) toExpire.add(lastVersion.sessionID)
    }
    for await (let id of toExpire){
        store.destroy(id)
        await File.unlink(`${SESSIONS_DIR}/${id}.ndjson`)
    }
}, 10000)

const files = await File.readdir(SESSIONS_DIR)

for await (let file of files){
    const data = await File.readFile(`${SESSIONS_DIR}/${file}`, 'utf-8')
    const lastLine = data.split('\n').filter(t => t.trim().length > 0).pop()
    const lastVersion = JSON.parse(lastLine)
    const expires = new Date(lastVersion.cookie.expires)
    if(expires && expires > Date.now()) {
        store.set(lastVersion.sessionID, lastVersion)
    }
}

export default async (robot, config) => {
    const validationDuringOauthSequence = new Map()
    const sessionParser = session({
        secret: config.COOKIE_SECRET,
        cookie: {
            secure: config.NODE_ENV == 'production',
            maxAge: 60*60*1000,
            sameSite: 'lax',
        },
        resave: false,
        saveUninitialized: false,
        name: 'jbot',
        rolling: true,
        store: store
    })

    if(!robot.protectedUrls) robot.protectedUrls = []
    robot.on('shutdown', ()=>{
        clearInterval(sessionsJob)
    })
    
    robot.authorize = (req, res, next)=>{
        if(req.session.user) {
            return next()
        }
        validationDuringOauthSequence.set(req.sessionID, {
            sessionId: req.sessionId,
            redirectUrl: req.url
        })
        res.redirect(`/login?redirectUrl=${encodeURIComponent(req.url)}`)
    }
    const app = await robot.setupExpress(config.PORT ?? 0)
    const wss = new WebSocketServer({noServer: true})
    robot.server.on('upgrade', (req, socket, head)=>{
        sessionParser(req, {}, ()=>{
            wss.handleUpgrade(req, socket, head, ws => {
                wss.emit('connection', ws, req)
            })
        })
    })
    const botName = robot.alias || robot.name

    wss.on('connection', (ws, req) => {
        if(!req.session.user) {
            ws.send(`HTTP/1.1 401 Unauthorized\r\n\r\n`, {isBinary: false})
            ws.close()
            return
        }

        let context = req.url.split('/').reduce((acc, current, i)=>{
            if(i == 2) acc.guild_id = current
            if(i == 3) acc.channel_id = current
            return acc
        }, {
            guild_id: null,
            channel_id: null,
            system: false,
            type: 0,
            content: '',
            author: {...req.session.user, bot: false, system: false}
        })
        ws.on('message', (data, isBinary)=>{
            const message = robot.adapter.createMessage(Object.assign(context, {
                content: `${botName} ${data.toString().replace(/^"/, '').replace(/"$/, '')}`
            }))
            robot.adapter.receive(message).then(_=>{
                robot.logger.info('recevied', message.text)
            }).catch(e => robot.logger.error('error receiving message from incoming request', e))

            wss.clients.forEach(client => {
                if(client == ws) return
                if(client.readyState != WebSocket.OPEN) return
                robot.logger.info('Broadcasting to all other clients.')
                client.send(data, {binary: isBinary})
            })
        })
    })

    app.disable('x-powered-by')
    express.static.mime.define({'text/javascript': ['js', 'mjs']})
    app.use(express.static('./public'))
    app.use('/lib', express.static('./lib'))
    app.use('/public/markdown', express.static('./node_modules/markdown-it/dist'))

    app.use(express.json({
        type: ['application/ld+json', 'application/json']
    }))

    app.use(express.urlencoded({ extended: true }))
    // app.use(MethodOverride("_method"))
    app.set('trust proxy', 1)
    app.use(sessionParser)
    app.use((req, res, next)=>{
        visitStream.push(new AddVisitEvent(req.sessionID, randomUUID(), new Date(), new Date(), req.sessionID, req.url, 0))
        next()
    })
    app.engine('html', async (filename, options, cb) => {
        const html = await File.readFile(filename, 'utf-8')
        const $ = cheerio(html)
        const props = {}
        $('[itemprop]').each((i, el)=>{
            const $el = $(el)
            let prop = $el.attr('itemprop')
            let content = $el.attr('content')
            if(prop == 'headline') {
                content = $el.text()
            }
            if(prop == 'published') {
                content = new Date($el.attr('datetime'))
            }
            props[prop] = content
        })
        props.uri = filename.replace(`${__dirname}/www`, '')
        props.relativeLink = props.uri.replace(/^\//, '')

        const template = handlebars.compile(html)
        cb(null, template({meta: props}), props)
    })
    app.engine('md', async (filePath, options, callback)=>{
        try{
            let data = await File.readFile(filePath, 'utf-8')
            let output = markdown.render(data)
            if(markdown.meta.layout){
                output = `{{#> ${markdown.meta.layout}}}\n${output}{{/${markdown.meta.layout}}}`
            }
            markdown.meta.uri = filePath.replace(`${__dirname}/www`, '').replace('.md', '.html')
            markdown.meta.relativeLink = markdown.meta.uri.replace(/^\//, '')
            markdown.meta.tags = markdown.meta?.tags ?? []
            let t = handlebars.compile(output)
            try{
                let stat = fs.statSync(filePath)
                markdown.meta.birthtime = stat.birthtime
            }catch(e){
                robot.logger.error(e)
            }
            if(markdown.meta.published) markdown.meta.displayDate = markdown.meta.published.toLocaleDateString(undefined, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' })
            if(markdown.meta.should_publish == 'yes') posts.push(markdown.meta)
            callback(null, t({meta: markdown.meta}), markdown.meta)
        }catch(e){
            callback(e, null)
        }
    })
    
    app.set('views', ['./www/'])
    app.set('view engine', 'html')
    app.get('/access-token', async (req, res, next)=>{
        if(!req.query.code) {
            return res.status(400).send('Bad Request')
        }
        const userFromOauth = Buffer.from(req.query.state, 'base64').toString('ascii').split(';').reduce((acc, current, i)=>{
            if(i == 0) acc.redirectUrl = current
            if(i == 1) acc.state = current
            if(i == 2) acc.entrySessionId = current
            return acc
        }, {})
        const user = validationDuringOauthSequence.get(userFromOauth.entrySessionId)
        if(!user || user.state != userFromOauth.state) return res.status(401).send('Unauthorized')
        validationDuringOauthSequence.delete(user.sessionID)
        const data = {
            client_id: config.DISCORD_CLIENT_ID,
            client_secret: config.DISCORD_CLIENT_SECRET,
            grant_type: 'authorization_code',
            redirect_uri: config.DISCORD_REDIRECT_URL,
            code: req.query.code
        }
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        const response = await fetch(`${config.DISCORD_API_HOST}/oauth2/token`, {
            method: 'POST',
            headers: headers,
            body: new URLSearchParams(data)
        })

        const token = await response.json()
        if(token.error) {
            return next(new Error(token.error))
        }
        const info = await fetch(`${config.DISCORD_API_HOST}/users/@me`, {
            headers: {
                Authorization: `Bearer ${token.access_token}`,
                Accept: 'application/json',
                'User-Agent': `Hubot (https://hubot.gokaizen.io, 1.0.0)`
            },
            method: 'GET'
        })
        if(info.status == 401) return res.status(401).send('Unauthorized: Access token')
        req.session.user = await info.json()
        res.redirect(userFromOauth.redirectUrl)
    })
    app.get('/login', async (req, res)=>{
        req.session.cookie.sameSite = 'lax'
        const redirectUrl = req.query.redirectUrl ?? '/'
        const randNumber = Math.random()
        const state = Buffer.from(`${decodeURIComponent(redirectUrl)};${randNumber};${req.sessionID}`).toString('base64')
        const user = {
            state: randNumber,
            redirectUrl,
            entrySessionId: req.sessionID
        }
        validationDuringOauthSequence.set(req.sessionID, user)
        res.render('login', {
            discordUrl: `${config.DISCORD_API_HOST}/oauth2/authorize?client_id=${config.DISCORD_CLIENT_ID}&redirect_uri=${encodeURIComponent(config.DISCORD_REDIRECT_URL)}&response_type=code&scope=identify&state=${state}`
        })
    })
    app.get('/logout', (req, res, next)=>{
        req.session.user = null
        req.session.save(err => {
            if(err) next(err)
            req.session.regenerate(err => {
                if(err) next(err)
                res.redirect('/')
            })
        })
    })

    const DOMAIN = 'fedi.devchitchat.com'
    const people = new Map()
    people.set(`joey@${DOMAIN}`, {
        subject: `acct:joey@${DOMAIN}`,
        links: [
            {
                rel: 'self',
                type: 'application/activity+json',
                href: `https://${DOMAIN}/@joey`
            }
        ]
    })

    const actors = new Map()
    const following = new Map()
    following.set(`joey@${DOMAIN}`, new Set())
    
    actors.set(`joey`, {
        '@context': 'https://www.w3.org/ns/activitystreams',
        id: `https://${DOMAIN}/@joey`,
        type: 'Person',
        name: 'Joey',
        preferredUsername: 'joey',
        summary: 'Joey is a developer and a member of the DevChitChat community.',
        inbox: `https://${DOMAIN}/users/joey/inbox`,
        outbox: `https://${DOMAIN}/users/joey/outbox`,
        followers: `https://${DOMAIN}/users/joey/followers`,
        following: `https://${DOMAIN}/users/joey/following`,
        liked: `https://${DOMAIN}/users/joey/liked`,
        publicKey: {
            id: `https://${DOMAIN}/users/joey#main-key`,
            owner: `https://${DOMAIN}/users/joey`,
            publicKeyPem: await File.readFile('./public.pem', 'utf8')
        }
    })

    class JRDLink {
        constructor(){
            this.rel = null
            this.type = null
            this.href = null
            this.titles = {}
            this.properties = {}
        }
    }
    class JRD {
        constructor(){
            this.subject = null
            this.links = []
            this.aliases = []
            this.properties = {}
        }
    }

    const getWebfinger = (req, res)=>{
        // if(req.get('accept').indexOf('application/jrd+json') == -1) {
        //     return res.status(406).end('Not Acceptable')
        // }
        if(!req.query?.resource) {
            return res.status(400).end('Bad Request')
        }

        res.type('application/jrd+json')

        const key = req.query?.resource.split('acct:').pop()
        if(key) {
            const person = people.get(key)
            if(!person) return res.status(404).end('Not Found')
            res.json(person)
        }else{
            res.status(404).end('Not Found')
        }
    }

    const getUser = (req, res)=>{
        const key = req.params.user
        const actor = actors.get(key)
        res.setHeader('Content-Type', 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
        if(!actor) return res.status(404).json({message: 'Not Found'})
        res.json(actor)
    }

    const getWellKnown = (req, res)=>{
        if(req.get('accept').indexOf('application/ld+json') == -1) return res.status(406).end('Not Acceptable')
        res.setHeader('Content-Type', 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
        res.send({
            "@context": "https://www.w3.org/ns/activitystreams",
            "id": "https://gokaizen.io/.well-known/webfinger",
            "type": "Service",
            "serviceEndpoint": "https://gokaizen.io/.well-known/nodeinfo"
        })
    }

    const inbox = new Map()
    const getInspect = (req, res)=>{
        res.json(Array.from(inbox.entries()))
    }

    const publicKey = await File.readFile('./public.pem', 'utf-8')

    const postToSharedInbox = async (req, res)=>{
        const activity = req.body
        if(!activity) return res.status(400).json({message: 'Bad Request: Missing body'})
        if(typeof activity !== 'object') return res.status(400).json({message: 'Bad Request'})
        const sigHeader = req.headers?.signature
        if(!sigHeader) return res.status(400).json({message: 'Bad Request: Missing Signature Header'})
        
        const fromSignature = sigHeader.split(',').reduce((acc, current)=>{
            const {key, value} = /(?<key>[^=]+)=\s?"?(?<value>[^"]+)"?\s?/.exec(current).groups
            acc.set(key, value)
            return acc
        }, new Map())

        const host = req.get('Host')
        const fromHeaders = getFromHeaders(fromSignature, activity.actor, req)
        const verified = verify(publicKey, fromHeaders, fromSignature.get('signature'))
        
        // TODO: Add a Digest to the headers for additional security
        const sentDateForReplayAttackPrevention = new Date(Date.parse(req.get('Date')))
        robot.logger.debug('shared inbox', verified, activity, fromHeaders)
        if(verified
        && (Date.now() - sentDateForReplayAttackPrevention) < 1000 * 3
        && activity.actor){
            robot.logger.debug('posted to shared inbox', JSON.stringify(activity))
            if(!inbox.has(activity.actor)) inbox.set(activity.actor, [])
            inbox.get(activity.actor).push(activity)
            return res.status(202).end()
        }
        res.status(417).end()
    }

    const outbox = new Map()
    const postToUsersOutbox = async (req, res)=>{
        const activity = req.body
        robot.logger.debug('posted to outbox', JSON.stringify(activity))
        if(!activity) return res.status(400).json({message: 'Bad Request: Missing body'})
        if(typeof activity !== 'object') return res.status(400).json({message: 'Bad Request'})
        const actor = actors.get(req.params.user)
        if(!actor) return res.status(404).json({message: 'Not Found'})
        if(activity.actor != actor.inbox) return res.status(403).json({message: 'Forbidden'})
        if(!outbox.has(activity.actor)) outbox.set(activity.actor, [])
        outbox.get(activity.actor).push(activity)
        res.status(202).end()
    }

    const postToUsersInbox = async (req, res) => {
        robot.logger.debug(req.headers, req.body)
        const activity = req.body
        if(!activity) return res.status(400).json({message: 'Bad Request: Missing body'})
        if(typeof activity !== 'object') return res.status(400).json({message: 'Bad Request'})
        
        const signatureFromHeaders = req.headers?.signature
        if(!signatureFromHeaders) return res.status(400).json({message: 'Bad Request: Missing Signature Header'})
        
        let signature = new Map()
        signatureFromHeaders.split(',').reduce((acc, current)=>{
            const {key, value} = /(?<key>[^=]+)=\s?"?(?<value>[^"]+)"?\s?/.exec(current).groups
            acc.set(key, value)
            return acc
        }, signature)

        const host = req.get('Host')
        const signatureHeaders = getFromHeaders(signature, activity.actor, req)

        robot.logger.debug('signatureFromHeaders', signatureFromHeaders, signatureHeaders, signature)

        const actorSigResponse = await fetch(signatureFromHeaders.keyId)
        robot.logger.debug(actorSigResponse.status, await actorSigResponse.text())
        const actorInSigature = await actorSigResponse.json()
        const actorsPublicKey = actorInSigature.publicKey.publicKeyPem      
        const verified = verify(actorsPublicKey, signatureHeaders, signature.get('signature'))
        
        // TODO: Add a Digest to the headers for additional security
        const sentDateForReplayAttackPrevention = new Date(Date.parse(req.get('Date')))
        robot.logger.debug('activity posted to inbox', activity)
        if(verified
        && (Date.now() - sentDateForReplayAttackPrevention) < 1000 * 3
        && activity.actor){
            robot.logger.debug('posted to users inbox', JSON.stringify(activity))
            if(!inbox.has(activity.actor)) inbox.set(activity.actor, [])
            inbox.get(activity.actor).push(activity)
            return res.status(202).end()
        }
        res.status(417).end()
    }

    app.get('/.well-known/', getWellKnown)
    app.get('/.well-known/webfinger', getWebfinger)
    app.get('/@:user', getUser)
    app.get('/:user', getUser)
    
    app.get('/inspect', getInspect)
    app.post('/inbox', postToSharedInbox)

    app.get('/users/:user', getUser)
    app.post('/users/:user/inbox', postToUsersInbox)
    app.post('/users/:user/outbox', postToUsersOutbox)


    let files = []
    const source = './www'
    const destination = './public'
    try{await File.mkdir(destination)}catch(e){}
    const folders = await File.readdir(source, {withFileTypes: true})
    
    robot.logger.info('Copy static files')
    await readFiles(folders, source, {
        async directoryWasFound(info){
            if(!['/layouts', '/pages'].some(folder => info.fileName.indexOf(folder) > -1)) {
                try{await File.mkdir(info.fileName.replace(source, destination))}catch(e){}
            }
            await readFiles(await File.readdir(info.fileName, {withFileTypes: true}), info.fileName, this)
        },
        async fileWasFound(info){
            if(info.fileName.indexOf('/layouts') == -1
                && info.fileName.indexOf('/pages') == -1
                && !info.fileName.endsWith('.md')) {
                createReadStream(info.fileName).pipe(createWriteStream(info.fileName.replace(source, destination)))
            }
            files.push(info.fileName)
        }
    })

    handlebars.registerHelper('compare', (a, b)=>{
        return a == b
    })
    handlebars.registerHelper('current', (a, b)=>{
        if(!a) return ''
        return a.endsWith(b) ? ' current' : ''
    })
    // register a helper that returns the difference between the length of the posts and the index
    handlebars.registerHelper('difference', (posts, index)=>{
        return posts.length - index - 1
    })

    robot.logger.info('Register partials')
    for await (let file of files){
        let data = await File.readFile(file, 'utf-8')
        let key = file.replace(`${source}/`, '').replace(/[\s|\-]/g, '_')
        if(key.indexOf('.html') > -1) {
            handlebars.registerPartial(key, data)
        }
    }

    const urls = []
    
    robot.logger.info(`Render the files`)
    for await (let file of files){
        if(file.indexOf('/layouts') > -1) continue
        if(!(file.endsWith('.html') || file.endsWith('.md'))) continue
        const key = file.replace(`${source}/`, '')
            .replace('.html', '')
            .replace('.md', '')

        let defaultEngine = file.split('.').pop()
        app.set('view engine', defaultEngine)
        let model = {}
        await (new Promise((resolve, reject)=>{
            app.render(key, model, async (err, html)=>{
                if(err) {
                    robot.logger.error('error rendering', err, file)
                    reject(err)
                    return
                }
                let name = file.replace(`${source}`, destination).replace('.md', '.html')
                name = name.replace('/pages', '')
                await File.writeFile(name, html, 'utf-8')
                urls.push(name.replace('./public', ''))
                resolve(html)
            })    
        }))
    }
    
    posts.sort((a, b)=>{
        return a.published > b.published ? 1 : 0
    })
    
    posts.reverse()

    const sitemap = await File.readFile(`${source}/sitemap.xml`, 'utf-8')
    const sitemapTemplate = handlebars.compile(sitemap)
    await File.writeFile(`${destination}/sitemap.xml`, sitemapTemplate({urls}))
    app.set('view engine', 'html')
    return app
}