import {createSign, createVerify, createHash} from 'node:crypto'


const sign = (privateKey, host, keyId, today = new Date(), digest = '', inbox = '/inbox')=>{
    const signer = createSign('SHA256')
    const input = `(request-target): post ${inbox}
host: ${host}
date: ${today.toUTCString()}
digest: ${digest}`

    signer.write(input)
    signer.end()
    const signedData = signer.sign(privateKey, 'base64')
    
    const signature = `keyId="${keyId}",algorithm="rsa-sha256",headers="(request-target) host date digest",signature="${signedData}"`
    return signature
}

const verify = (publicKey, input, signature)=>{
    const verifier = createVerify('SHA256')
    verifier.write(input)
    verifier.end()
    const verified = verifier.verify(publicKey, signature, 'base64')
    return verified
}

const hash = (input)=>{
    return createHash('sha256').update(input).digest('base64')
}

const getFromHeaders = (incomingHeaderSignature, uri, headers)=>{
    return incomingHeaderSignature.get('headers').split(' ').map(key => {
        if(key == '(request-target)') return `${key}: post ${uri}`
        else return `${key}: ${key == 'digest' ? headers.get(key).match(/(?<key>[^=]+)=\s?"?(?<value>[^"]+)"?\s?/).groups.value : headers.get(key)}`
    }).join('\n')
}

export {sign, hash, verify, getFromHeaders}