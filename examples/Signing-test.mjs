import {describe, it} from 'node:test'
import assert from 'node:assert/strict'
import {sign, hash, verify} from '../lib/HttpSignature.mjs'
import File from 'node:fs/promises'

const DOMAIN = 'fedi.devchitchat.com'

describe('Sign and verify', ()=>{    
    it('Sign with private key, verify with public key', async ()=>{
        const today = new Date()
        const publicKey = await File.readFile('./public.pem', 'utf-8')
        const privateKey = await File.readFile('./private.pem', 'utf-8')
        const digest = hash('Hello world')
        const incomingHeaderSignature = sign(privateKey, DOMAIN, `https://${DOMAIN}/joey#main-key`, today, digest)
        .split(',').reduce((acc, current)=>{
            const {key, value} = /(?<key>[^=]+)=\s?"?(?<value>[^"]+)"?\s?/.exec(current).groups
            acc.set(key, value)
            return acc
        }, new Map())

        const headers = new Map()
        headers.set('host', DOMAIN)
        headers.set('date', today.toUTCString())
        headers.set('digest', `SHA-256=${digest}`)
        headers.set('signature', incomingHeaderSignature.get('signature'))

        const fromRequestHeaders = incomingHeaderSignature.get('headers').split(' ').map(key => {
            if(key == '(request-target)') return `${key}: post /inbox`
            else return `${key}: ${key == 'digest' ? headers.get(key).match(/(?<key>[^=]+)=\s?"?(?<value>[^"]+)"?\s?/).groups.value : headers.get(key)}`
        }).join('\n')

        const verified = verify(publicKey, fromRequestHeaders, incomingHeaderSignature.get('signature'))
        assert.deepEqual(verified, true)
    })
})