import {describe, it} from 'node:test'
import assert from 'node:assert/strict'
import {Robot} from 'hubot'

const PATH_TO_SHELL = '../../../../../../src/adapters/Shell.mjs'
describe('Webfinger protocol', async ()=>{
    it('The query component MUST contain a "resource" parameter', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known/webfinger`, {
            method: 'GET',
            headers: {
                'Accept': 'application/jrd+json'
            }
        })
        const expected = 400
        const actual = response.status
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })

    it('MUST return a JRD representation', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known/webfinger?resource=acct:joey`, {
            method: 'GET',
        })
        
        const expected = 'application/jrd+json'
        const actual = response.headers.get('content-type')
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })

    it('MAY contain one or more "rel" parameters', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known/webfinger?resource=acct:joey&rel=${encodeURIComponent('http://webfinger.example/rel/profile-page')}`, {
            method: 'GET',
        })
        
        const expected = 404
        const actual = response.status
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })
    it('Returns an account in JRD representation', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known/webfinger?resource=acct:joey@fedi.devchitchat.com`, {
            method: 'GET',
            headers: {
                'Accept': 'application/jrd+json'
            }
        })
        const expected = {
            "subject": `acct:joey@fedi.devchitchat.com`,
            "links": [
                {
                    "rel": "self",
                    "type": "application/activity+json",
                    "href": `https://fedi.devchitchat.com/@joey`
                }
            ]
        }
        const actual = await response.json()
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })
})