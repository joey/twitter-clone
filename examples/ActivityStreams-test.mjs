import {describe, it} from 'node:test'
import assert from 'node:assert/strict'
import {Robot} from 'hubot'
import {sign, hash, getFromHeaders} from '../lib/HttpSignature.mjs'
import File from 'node:fs/promises'

const PATH_TO_SHELL = '../../../../../../src/adapters/Shell.mjs'
const DOMAIN = 'fedi.devchitchat.com'

const publicKey = await File.readFile('./public.pem', 'utf-8')
const privateKey = await File.readFile('./private.pem', 'utf-8')

describe('ActivityPub', ()=>{
    it.skip('For when a server needs to GET an actor (e.g. @joey)', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()

        const response = await fetch(`http://localhost:${robot.port}/@joey`, {
            method: 'GET',
            headers: {
                'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
            }
        })

        const expected = {
            '@context': 'https://www.w3.org/ns/activitystreams',
            id: 'https://fedi.devchitchat.com/@joey',
            type: 'Person',
            name: 'Joey',
            preferredUsername: 'joey',
            summary: 'Joey is a developer and a member of the DevChitChat community.',
            inbox: `https://${DOMAIN}/users/joey/inbox`,
            outbox: `https://${DOMAIN}/users/joey/outbox`,
            followers: `https://${DOMAIN}/users/joey/followers`,
            following: `https://${DOMAIN}/users/joey/following`,
            liked: `https://${DOMAIN}/users/joey/liked`,
            publicKey: {
                id: `https://${DOMAIN}/users/joey#main-key`,
                owner: `https://${DOMAIN}/users/joey`,
                publicKeyPem: '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA31m2B1q3a9KKNimAYM8A\na880juXwM4XXRPGd73YvOZdETN2qcZRb+yd1VRGVdrs1C8YrVfGIKK8mjKUdqp8X\nlbxspL97rIHvtYdJz/uXGPVE49Idq++ihZdEME7YQa1SBHR3OzNJrGFYH8rTQg/D\nl93k56nHHGSrAOayTJbeMgapmfA+0oBSRkBmrSSKNv7ClPYMaJv2FIMHRS2WaXoR\nuGxonIfkqfKToTj+IwiP6dqponB6AaAtNg/JNhQ6c5xeemVOZHsq2LhPTq9EBWAZ\ntb4maYsvfuWXJ5jXm5IHM2ISzG2tGknjKl0ZqrA7LWAdw27fLWSL7t/UzkYeRooR\n2QIDAQAB\n-----END PUBLIC KEY-----\n'
            }
        }
        const actual = await response.json()
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })
    
    it.skip('POSTs a message to the servers inbox, signature is verified', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const today = new Date()

        const doc = {
            '@context': 'https://www.w3.org/ns/activitystreams',
            id: `http://localhost:${robot.port}/posts/create-hello-world`,
            type: 'Create',
            actor: `http://localhost:${robot.port}/joey`,
            object: {
                id:`http://localhost:${robot.port}/posts/create-hello-world`,
                type: 'Note',
                published: (new Date()).toUTCString(),
                attributedTo: `http://localhost:${robot.port}/joey`,
                inReplyTo: 'https://mastodon.social/@Gargron/100254678717223630',
                content: 'Hello world!',
                to: 'https://www.w3.org/ns/activitystreams#Public'
            }
        }
        const digest = hash(JSON.stringify(doc))
        const keyId = `https://${DOMAIN}/users/joey#main-key`
        const signature = sign(privateKey, DOMAIN, keyId, today, digest)

        const response = await fetch(`http://localhost:${robot.port}/inbox`, {
            method: 'POST',
            headers: {
                'Host': 'fedi.devchitchat.com',
                'Date': today.toUTCString(),
                'Digest': digest,
                'Signature': signature,
                'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
            },
            body: JSON.stringify(doc)
        })
        const expected = 202
        const actual = response.status
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })

    it.skip('Accept application/ld+json', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known/`, {
            method: 'GET',
            headers: {
                'Accept': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
            }
        })
        const expected = 200
        const actual = response.status
        const json = await response.json()
        robot.shutdown()
        assert.deepEqual(json['@context'], 'https://www.w3.org/ns/activitystreams')
        assert.deepEqual(actual, expected)
    })

    it.skip('Other accept headers are rejected', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()
        const response = await fetch(`http://localhost:${robot.port}/.well-known`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            }
        })
        const expected = 406
        const actual = response.status
        robot.shutdown()
        assert.deepEqual(actual, expected)
    })

    it('Follow somone', async ()=>{
        const robot = new Robot()
        await robot.loadAdapter(PATH_TO_SHELL)
        robot.run()

        const today = new Date()
        const payload = {
            '@context': 'https://www.w3.org/ns/activitystreams',
            id: `https://${DOMAIN}/my-first-follow`,
            type: 'Follow',
            actor: `https://${DOMAIN}/@joey`,
            object: 'https://mastodon.social/users/joeyguerra'
        }
        const digest = hash(JSON.stringify(payload))
        const keyId = `https://${DOMAIN}/users/joey#main-key`
        const signature = sign(privateKey, DOMAIN, keyId, today, digest, `https://${DOMAIN}/@joey`)
        robot.logger.debug('from test', JSON.stringify(signature), today.toUTCString())
        robot.logger.debug('digest', digest)
    
        const response = await fetch(`https://mastodon.social/inbox`, {
            method: 'POST',
            headers: {
                'Host': `mastodon.social`,
                'Date': today.toUTCString(),
                'Digest': `sha-256=${digest}`,
                'Signature': signature,
                'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
            },
            body: JSON.stringify(payload)
        })

        robot.logger.debug('response from fetch', response.status, response.headers)
        const json = await response.text()
        robot.logger.debug(response.status, json)
        const expected = 200
        const actual = 200
        robot.shutdown()
        assert.deepEqual(actual, expected)

    })
})