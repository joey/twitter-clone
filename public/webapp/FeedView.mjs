import { makeKeyValueObservable } from '/lib/Observable.mjs'
// import {Events} from '../../../../lib/Models.mjs'
// import NotificationCenter from '../../../../lib/NotificationCenter.mjs'

function hookGithubResponse(message){
    try{
        var users = JSON.parse(message.text)
        if(users.what === 'github list of users'){
            message.text = '<ul>'
            users.items.forEach(function(user){
                message.text += '<li><a href="' + user.html_url + '"><img class="img-circle avatar" src="' + window.location.origin + user.avatar_url + '" /></a></li>'
            })
            message.text += '</ul>'
        }
    }catch(e){
    }
    return message
}
function hookListOfUsers(message){
    try{
        var users = JSON.parse(message.text)
        if(users.what === 'list of users'){
            message.text = '<ul>'
            for(key in users){
                if(!users[key].avatar) continue
                message.text += '<li><img class="img-circle avatar" src="' + window.location.origin + users[key].avatar + '" /></a></li>'
            }
            message.text += '</ul>'
        }
    }catch(e){
    }
    return message
}
function hookGsearchResultClass(message){
    if(message.text.indexOf('GsearchResultClass') === -1) return message
    var result = JSON.parse(message.text)
    var searchResult = result.responseData.results
    message.text = ''
    searchResult.forEach(function(s){
        message.text += '<img src="{src}" width="200" />'.replace(/{src}/, s.unescapedUrl)
    })
    return message
}

function hookForDataImage(message){
    if(message.text.indexOf('data:image') > -1){
        message.text = `![](${message.text})`
    }
    return message
}

class FeedView {
    #md
    constructor(container, model, delegate){
        this.container = container
        this.model = model
        this.delegate = delegate
        this.template = this.container.querySelector('.post')
        this.lastTimeMessageWasSent = (new Date()).getTime()
        this.#md = this.delegate.window.markdownit()
        this.hooks = [
            hookForDataImage,
            hookGsearchResultClass,
            hookGithubResponse,
            hookListOfUsers,
            message => {
                message.text = this.#md.render(message.text)
                return message
            }
        ]
		this.model.observe('push', this.messageWasAdded.bind(this))
		this.model.observe('pop', this.messageWasRemoved.bind(this))
    }
    message(message){
        if(!message) return
        if(message.text.trim().length == 0) return
        if(this.delegate && this.delegate.messageWasReceived){
            this.delegate.messageWasReceived(message)
        }
        this.model.push(makeKeyValueObservable(message))
    }
    messageWasAdded(key, old, v){
        if(!v) return
        if(!v.from) return
        const elem = this.template.cloneNode(true)
        elem.style.display = ''
        elem.addEventListener('dblclick', this.delegate.messageWasDoubleClicked.bind(this.delegate), true)
        this.hooks.forEach(hook => {
            v = hook(v)
        })
        if(this.delegate.window.member.id == v.from.id){
            elem.className = elem.className.split(' ').concat(['self']).join(' ')
        }
        const first = this.container.querySelector('.post:not([style*="display: none;"])')
        elem.querySelector('.text').innerHTML = v.text
        elem.querySelector('img').src = v.from.avatar
        elem.querySelector('figcaption').innerHTML = v.from.displayName
        elem.querySelector('time').setAttribute('datetime', v.occurred)
        elem.querySelector('time').innerHTML = new Date(v.occurred).toLocaleString()
        this.container.insertBefore(elem, first)
        this.lastTimeMessageWasSent = v.time
        // NotificationCenter.publish(Events.CHAT_HEIGHT_HAS_CHANGED, this, this.feed.scrollHeight - originalHeight)
    }
    messageWasRemoved(key, old, v){
        const last = this.container.querySelector('.list:last-child')
        this.container.removeChild(last)
    }
}

export default FeedView