import {HybridLogicalClock} from '/lib/HybridLogicalClock.mjs'
import EntryView from './EntryView.mjs'
import FeedView from './FeedView.mjs'
import PreviewView from './PreviewView.mjs'

const pipe = (...fns) => x => fns.reduce((y, f) => f(y), x)

const findParentWith = (node, predicate) => {
    if(!node) return null
    if(predicate(node)) return node
    return findParentWith(node.parentNode, predicate)
}

class App {
    #counter = 0
    constructor(window, socket, model){
        this.window = window
        this.db = this.window.conn.db
        this.model = model
        this.socket = socket ?? {
            emit: (event, message) => console.log('socket.emit', event, message),
            on: (event, callback) => console.log('socket.on', event, callback)
        }
        this.views = []
        this.subscriptions = {}
        this.socket.on('update', this.update.bind(this))
        this.socket.on('message', this.message.bind(this))
        this.socket.on('joined', this.joined.bind(this))

        this.views.push(new FeedView(document.getElementById('feed'), this.model.messages, this))
        this.views.push(new PreviewView(document.getElementById('preview'), this.model.message, this))
        this.views.push(new EntryView(document.getElementById('entry'), this.model.message, this))
    }
    messageWasSubmitted(message){
        this.model.messages.push(message)
        const hlc = new HybridLogicalClock()
        const now = Date.now()
        const clock = hlc.init(now.toString(), 'local')
        clock.count += ++this.#counter
        const id = hlc.serialize(clock).replace(/:/g, '-')
        const transaction = this.db.transaction(['messages'], 'readwrite')
        transaction.onerror = e => console.error('error adding message', e)
        const store = transaction.objectStore('messages')
        store.add({id: id, text: message.text, from: {
            id: message.from.id,
            displayName: message.from.displayName,
            avatar: message.from.avatar
        }, clock: clock})
        store.onsuccess = e => console.log('added message', e)
    }
    messageWasDoubleClicked(e){
        this.views.forEach(v => {
            if(v.messageWasDoubleClicked){
                const fromId = findParentWith(e.target, node => node.getAttribute('data-from'))?.getAttribute('data-from')
                const from = this.model.roster.find(u => {
                    return fromId === u.id
                })
                v.messageWasDoubleClicked({text: e.target.innerHTML, from: from})
            }
        })
    }
    message(message){
        console.log('got message from message: ', message.substr(0, 20))
        this.model.messages.push(message)
    }
    update(message){
        console.log('got message from update: ', message.substr(0, 20))
        if(!message) return
        if(message.length == 0) return
        if(message.indexOf('<html') == -1) return
        window.location.reload(true)
        const parser = new DOMParser()
        const doc = parser.parseFromString(message, 'text/html')
        this.window.document.body = doc.body
        this.window.document.open()
        this.window.document.write(message)
        this.window.document.close()
    }
    joined(message){
        console.log('Someone just joined', message.member.id, this.model.self.id)
    }
    send(message){
        this.socket.emit('sending message from user', message)
    }
}
export default App